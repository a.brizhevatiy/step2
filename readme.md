Проект - Step2, проектная работа по завершению раздела обучения "Продвинутый курс HTML" DanIT, Киев
Исполнители:
Лелюх Катерина - разделы Revolutionary Edition, Pricing.
Брижеватый Александр - разделы Head/Fork, Pepole.

Используемые пакеты:
Npm,
gulp,
gulp-concat - для объединения файлов,
gulp-autoprefixer - прописывание поддержки браузеров,
gulp-terser - оптимизатор JS кода,
gulp-clean-css - оптимизатор CSS кода,
gulp-imagemin - оптимизатор изображений,
gulp-sass - препроцессор,
del - удаление файлов,
browser-sync - локальный сервер,
gulp-notify - форматирование уведомлений,
gulp-plumber - не прерывает работу Gulp при ошибке.
