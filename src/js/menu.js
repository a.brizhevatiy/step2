const menuButton = $('.header__nav-button');
const headerNav = $('.header-navigation');
$(menuButton).on('click', function () {
    $(headerNav).fadeToggle();
})

$(window).resize(function () {
    if (window.innerWidth >= 481 && $(headerNav).css('display') === 'none') {
        $(headerNav).fadeIn(100);
    } else if (window.innerWidth < 481 && $(headerNav).css('display') !== 'none') {
        $(headerNav).fadeOut(100);
    }
})