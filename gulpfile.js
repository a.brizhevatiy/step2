const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const terser = require('gulp-terser'); //for JS min
const cleanCss = require('gulp-clean-css');
const imgmin = require('gulp-imagemin');
const sass = require('gulp-sass');
const del = require('del');
const browserSync = require('browser-sync');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');


const stileFiles=[
    "./node_modules/normalize.css/normalize.css",
    "./src/scss/main.scss"
];

const scriptFiles=[
    "./src/js/menu.js",
]

function scssFiles() {
    return gulp
        .src(stileFiles)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCss({
            level: 2,
        }))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
}

function jsFiles() {
    return gulp
        .src(scriptFiles)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(concat('scripts.min.js'))
        .pipe(terser())
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream());
}

function imgFiles() {
    return gulp
        .src('./src/img/**/*')
        .pipe(imgmin())
        .pipe(gulp.dest('./dist/img'))
        .pipe(browserSync.stream());
}

function remove() {
    return del(['./dist/*']);
}

function watch(){
    browserSync.init({
        server: {
            baseDir: './'
        }
    });

    gulp.watch("./src/scss/**/*.scss", scssFiles);
    gulp.watch("./src/js/**/*.js", jsFiles);
    gulp.watch("./src/img/**/*", imgFiles);
    gulp.watch("./*.html", browserSync.reload)
}

gulp.task('styles', scssFiles);
gulp.task('scripts', jsFiles);
gulp.task('images', imgFiles);
gulp.task('remove', remove);
gulp.task('watch', watch);

gulp.task('build', gulp.series(remove, gulp.parallel(scssFiles, jsFiles, imgFiles)));
gulp.task('dev', gulp.series('build', watch));

